;;; hugo-template-mode.el --- Major mode for Hugo templates -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Version: 0.0.0
;; Package-Requires: ((emacs "25.1"))
;; Homepage: https://kisaragi-hiu.com/projects/hugo-template-mode
;; Keywords: languages hugo


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; There doesn't seem to be a major mode for Hugo templates. I hope to
;; fix that.

;; TODO: perhaps we need a better name for the package

;;; Code:

;; TODO: highlighting
;; TODO: completion
;; TODO: starting in Hugo projects
(define-minor-mode hugo-shortcode-mode
  "Minor mode for Hugo shortcodes."
  :lighter "Hugo-Shortcode" :init-value nil
  (if hugo-shortcode-mode
      (progn
        (yas-activate-extra-mode 'hugo-shortcode-mode))
    (yas-deactivate-extra-mode 'hugo-shortcode-mode)))

(defconst hugo-template-keywords
  '("block"
    "define"
    "else if"
    "else"
    "end"
    "if"
    "range"
    "template"
    "with"))

(defconst hugo-template-functions-math
  '("add"
    "sub"
    "mul"
    "div"
    "mod"
    "modBool"
    "math.Ceil"
    "math.Floor"
    "math.Round"
    "math.Log"
    "math.Sqrt"
    "math.Pow"
    "ge"
    "gt"
    "le"
    "lt"
    "ne"))

(defconst hugo-template-functions-image
  '("imageConfig"
    "images.Brightness"
    "images.ColorBalance"
    "images.Colorize"
    "images.Contrast"
    "images.Filter"
    "images.Gamma"
    "images.GaussianBlur"
    "images.Grayscale"
    "images.Hue"
    "images.Invert"
    "images.Overlay"
    "images.Pixelate"
    "images.Pixelate"
    "images.Saturation"
    "images.Sepia"
    "images.Sigmoid"
    "images.UnsharpMask"))

(defconst hugo-template-functions
  `(,@hugo-template-functions-math
    ,@hugo-template-functions-image
    "absLangURL"
    "absURL"
    "after"
    "anchorize"
    "append"
    "apply"
    "base64"
    "chomp"
    "complement"
    "cond"
    "countrunes"
    "countwords"
    "dateFormat"
    "default"
    "delimit"
    "dict"
    "echoParam"
    "emojify"
    "eq"
    "errorf"
    "warnf"
    "fileExists"
    "findRE"
    "first"
    "float"
    "getenv"
    "group"
    "hasPrefix"
    "highlight"
    "hmac"
    "htmlEscape"
    "htmlUnescape"
    "hugo"
    "humanize"
    "i18n"
    "in"
    "index"
    "int"
    "intersect"
    "isset"
    "jsonify"
    "lang.Merge"
    "lang.NumFmt"
    "last"
    "len"
    "lower"
    "markdownify"
    "md5"
    "merge"
    "now"
    "os.Stat"
    "partialCached"
    "path.Base"
    "path.Dir"
    "path.Ext"
    "path.Join"
    "path.Split"
    "plainify"
    "pluralize"
    "print"
    "printf"
    "println"
    "querify"
    "readDir"
    "readFile"
    "ref"
    "reflect.IsMap"
    "reflect.IsSlice"
    "relLangURL"
    "relURL"
    "relref"
    "replace"
    "replaceRE"
    "safeCSS"
    "safeHTML"
    "safeHTMLAttr"
    "safeJS"
    "safeURL"
    "seq"
    "sha"
    "shuffle"
    "singularize"
    "slice"
    "slicestr"
    "sort"
    "split"
    "string"
    "strings.Count"
    "strings.HasSuffix"
    "strings.Repeat"
    "strings.RuneCount"
    "strings.TrimLeft"
    "strings.TrimPrefix"
    "strings.TrimRight"
    "strings.TrimSuffix"
    "substr"
    "symdiff"
    "templates.Exists"
    "time"
    "title"
    "transform.Unmarshal"
    "trim"
    "truncate"
    "union"
    "uniq"
    "upper"
    "urlize"
    "urls.Parse"
    "where")
  "List of all builtin template functions.

I consider `range' and `with' keywords.")

(defconst hugo-template-mode-font-lock-keywords
  `(
    ("\\({{\\|}}\\)" . (1 font-lock-constant-face t))
    ("{{.*\\(\".*\"\\).*}}" . (1 font-lock-string-face t))
    (,(format (rx "{{"
                  (opt (any "<" "-"))
                  (opt " ")
                  (group (*? any))
                  " "))
     . (1 font-lock-function-name-face t))
    ;; keywords are more specific than function names
    (,(format (rx "{{"
                  (opt (any "<" "-"))
                  (opt " ")
                  "%s"
                  (0+ any)
                  (opt " ")
                  (opt (any ">" "-"))
                  "}}")
              (regexp-opt hugo-template-keywords 'words))
     . (1 font-lock-keyword-face t))))

;; TODO: syntax
(define-minor-mode hugo-template-mode
  "Major mode for editing Hugo templates."
  :init-value nil :lighter "Hugo Template"
  (if hugo-template-mode
      (progn
        ;; `setq-local' only started accepting more than 1 pair since Emacs 27
        (setq-local comment-start "{{/*")
        (setq-local comment-end "*/}}")
        (font-lock-add-keywords nil hugo-template-mode-font-lock-keywords 'append)
        (font-lock-flush)
        (yas-activate-extra-mode 'hugo-template-mode))
    ;; FIXME: comment-start and comment-end not restored
    (yas-deactivate-extra-mode 'hugo-template-mode)
    (font-lock-remove-keywords nil hugo-template-mode-font-lock-keywords)
    (font-lock-flush)))

;; FIXME: remember to get rid of this
(add-hook 'web-mode-hook #'hugo-template-mode)

(when (featurep 'yasnippet)
  (defvar yas-snippet-dirs)
  (add-to-list 'yas-snippet-dirs (expand-file-name "snippets" (file-name-directory (locate-library "hugo-template-mode"))))
  (yas-reload-all))

(provide 'hugo-template-mode)

;;; hugo-template-mode.el ends here
