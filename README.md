# hugo-template-mode

There isn’t an Emacs mode for editing Hugo templates. This package aims to fix that.

## Prior art

- https://github.com/AdamNiederer/vue-html-mode/
- https://github.com/theNewDynamic/language-hugo-vscode/
- https://github.com/mattstratton/language-hugo/
- https://github.com/lijunsong/pollen-mode

## Goals

- [ ] Editing facilities and highlighting for Hugo templates
- [ ] Highlighting for Hugo shortcodes

# License

GPLv3.
